<?php
include_once("includes/connect.php");
if (!is_numeric($_GET['id'])) {
    header("location:index");
    exit;
}
$page_id = $_GET['id'];
$select_query = "select *, (select cat_title from categories where cat_id = news.cat_id) as cat_name, (select url_link from categories where cat_id = news.cat_id) as cat_url from news where news_id = '$page_id'";
$run = mysqli_query($dbcon, $select_query);//here run the sql query.
$row = mysqli_fetch_array($run); //while look to fetch the result and store in a array $row.
$news_id = $row['news_id'];
$news_title = $row['news_title'];
$news_date = $row['news_date'];
$news_author = $row['news_author'];
$news_image = $row['news_image'];
$news_content = $row['news_content'];
$cat_title = $row['cat_name'];
$description = $row['short_desc'];
$key_words = $row['news_keywords'];
$src = $row['ref_link'];
$src_link = $row['ref_web'];
$news_read = $row['news_read'];
$url_link = $row['cat_url'];
if ($row['news_state'] == 2)
    mysqli_query($dbcon, "update news set news_read = news_read + 1 where news_id = '$page_id'") or die(mysqli_error($con));

$page = 1;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $news_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $key_words; ?>">
    <meta name="description" content="<?php echo $description; ?>">
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo $news_title; ?>"/>
    <meta property="fb:app_id" content="1303916186392315"/>
    <meta property="og:description" content="<?php echo $description; ?>"/>
    <meta property="og:image" content="//cocis.news/images/news/<?php echo $news_image; ?>">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="528">
    <meta property="og:image:alt" content="<?php echo $news_title; ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<!---nav-bar-header-->
<?php
require_once("header.php");
?>
<!----//header----------->
<!------section---------->
<?php
include_once("leftbar.php");
?>
<!-------middle----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">
                <ol class="breadcrumb">
                    <li><a href="<?php base_url(); ?>index"><i class="fa fa-home"></i>Home<i
                                    class="fa fa-angle-right"></i></a></li>
                    <li><a href="<?php base_url(); ?>page/<?php echo $url_link; ?>">News<i
                                    class="fa fa-angle-right"></i></a></li>
                    <li class="active"><?php echo $cat_title; ?> <i class="fa fa-angle-right"></i></li>
                    <li><a href="//<?php echo $src_link ?>"><?php echo $src; ?></a></li>
                    <li><i class="fa fa-caret-right"></i> <?php echo $news_read; ?> read(s)</li>
                </ol>
                <h2 class="post_title wow "><?php echo $news_title; ?></h2>
                <a class="author_name"><i class="fa fa-user"></i><?php echo $news_author; ?></a>
                <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $news_date; ?></a>
                <div class="single_post_content">
                    <img class="img-center" src="<?php base_url(); ?>images/news/<?php echo $news_image; ?>" alt="">
                    <p align="justify" class="content"><?php echo $news_content; ?></p>
                    <hr>
                    <a href="whatsapp://send?text= <?php echo $description; ?> https://<?php echo $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI']; ?>  ">
                        <button class="btn btn-success fa fa-whatsapp">Share on whatsapp</button>
                    </a>
                    <div class="fb-share-button"
                         data-href="https://<?php echo $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI']; ?>"
                         data-layout="button_count" data-size="small" data-mobile-iframe="true">
                        <a class="fb-xfbml-parse-ignore" target="_blank"
                           href="https://www.facebook.com/sharer/sharer.php?u=https://<?php echo urlencode($_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI']); ?>&amp;src=sdkpreparse">Share</a>
                    </div>
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1303916186392315";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                </div>
                <div class="post_footer">
                    <div id="disqus_thread"></div>
                    <script>
                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                        /*
                        var disqus_config = function () {
                        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };
                        */
                        (function () { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://cocis-news.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript style="visibility: hidden">Please enable JavaScript to view the <a
                                href="https://disqus.com/?ref_noscript">comments
                            powered by Disqus.</a></noscript>

                </div>
                <div class="social_area wow fadeInLeft">
                    <ul>
                        <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a>
                        </li>
                        <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li><a href="https://plus.google.com/u/1/communities/108139383578981516178"
                               target="_blank"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <li><a href="https://www.pinterest.com/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
                <div class="related_post">
                    <h2 class="wow fadeInLeftBig">You may also like <i class="fa fa-thumbs-o-up"></i></h2>
                    <ul class="recentpost_nav relatedpost_nav wow fadeInDown animated">
                        <?php include("pages/celebrity-corner.php"); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--start-trending-stories-->
<?php
include_once("right-bar.php");
?>

<!--------//section------->
<!-------footer----------->
<?php
require_once("footer.php");
?>
<!-----//footer--------->
 