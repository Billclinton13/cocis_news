<?php
include_once("../header.php");
?>
    <!----//header----------->
    <section id="errorpage_body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="single_category wow fadeInDown">
                    <div class="category_title"><a href="#">WELCOME TO COCIS NEWS VIDEO NEST</a></div>
                    <br/>
                    <!--level-1-->
                    <div class="category_title"><a href="#">Latest Videos</a></div>
                    <?php include_once("videos/popular.php");
                    videos_by_category(1, $dbcon); ?>
                </div>
                <!--//level-1-->
                <!--level-2-->
                <div class="category_title"><a href="#">Most Popular Videos</a></div>
                <?php videos_by_category(2, $dbcon); ?>
            </div>
            <!--//level-2-->
            <!--level-3-->
            <div class="category_title"><a href="#">Best and Most Viewed Videos</a></div>
            <?php videos_by_category(3, $dbcon); ?>
        </div>
        <!--//level-3-->
        <!--level-4-->
        <div class="category_title"><a href="#">Trending Videos</a></div>
        <?php videos_by_category(4, $dbcon); ?>
        </div>
        <!--//level-4-->
    </section>
    <!--//section-->
    <!-------footer----------->
<?php
include_once("../footer.php");
?>