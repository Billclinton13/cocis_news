<?php
include_once "pages/agents.php";
$ua = getBrowser();
include_once "pages/visitor.php";
function base_url()
{
    echo "//" . $_SERVER['SERVER_NAME'] . "/";
}

function categories($con)
{
    $query = mysqli_query($con, "select * from categories where cat_state  = 2 order by cat_id asc limit 2") or die(mysqli_error($con));
    while ($info = mysqli_fetch_array($query)) {
        echo "<ii> <a href=\"pages/latest?id=" . $info['cat_id'] . "\">" . $info['cat_title'] . " </a> </li>\n";
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <title>Cocis News</title>
    <meta name="keywords"
          content="Cocis News, cocis, Makerere News, Campass News, Campass Bee, Matooke Republic, Tumusiime Ashiraff, College of computing and information science, , Scit, Ug hostels, Tumusiime Ashan, Clinton Nkesiga"/>
    <meta name="description" content="Getting you entertained and informed with accurate and first hand information"/>
    <?php
    if (!isset($page)) {
        echo "
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:title\" content=\"Cocis News\"/>
    <meta property=\"fb:app_id\" content=\"1303916186392315\" />
    <meta property=\"og:description\" content=\"Getting you informed and entertained with accurate and first hand information\"/>
    <meta property=\"og:image\" content=\"https://cocis.news/tech1.jpg\">
    ";
    }
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="We get you covered, inspired and informed always.Lock in for news">
    <!--style and bootstrap-->
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php base_url(); ?>assets/css/button.css">
    <link rel="icon" href="<?php base_url(); ?>favicon.ico">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body onLoad="startclock(); timerONE=window.setTimeout" TEXT="ffffff">

<!--start-top-header-->
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    <!--social-icons-->
    <div class="footer_bottom_right">
        <ul>
            <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a></li>
            <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a></li>
            <li><a href="https://plus.google.com/u/1/communities/108139383578981516178" target="_blank"><span
                            class="fa fa-google-plus"></span></a></li>
            <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a></li>
            <li><a href="https://www.pinterest.com/" target="_blank"><span class="fa fa-pinterest"></span></a></li>
        </ul>
    </div>
    <!--//social-icons-->

    <!---counter---->
    <div style="visibility:hidden">
    </div>
    <!---//counter--->
    <!-----clock-------->
    <script LANGUAGE="JavaScript">

        var timerID = null;
        var timerRunning = false;

        function stopclock() {
            if (timerRunning)
                clearTimeout(timerID);
            timerRunning = false;
        }

        function showtime() {
            var now = new Date();
            var hours = now.getHours();
            var minutes = now.getMinutes();
            var seconds = now.getSeconds()

            var timeValue = "" + ((hours > 12) ? hours - 12 : hours)
            timeValue += ((minutes < 10) ? ":0" : ":") + minutes
            timeValue += ((seconds < 10) ? ":0" : ":") + seconds
            timeValue += (hours >= 12) ? " P.M." : " A.M."
            document.clock.face.value = timeValue;

            timerID = setTimeout("showtime()", 1000);
            timerRunning = true;
        }

        function startclock() {
            //Make sure the clock is stopped
            stopclock();
            showtime();
        }
    </script>
    <div class="clock" style="margin-left: 0">
        <form name="clock" onSubmit="0">
            <label>TIME</label>
            <input type="text" name="face" size=13 value=""></form>
    </div>
    <!-----//clock-->
    <!---nav-bar-header-->
    <div class="box_wrapper">
        <div id="header">
            <div class="header_top">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span
                                        class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                                        class="icon-bar"></span> <span class="icon-bar"></span></button>
                        </div>
                        <!--menu-->
                        <div id="navbar" class="navbar-collapse collapse ">
                            <ul class="nav navbar-nav custom_nav">
                                <li><a href="<?php base_url(); ?>index">Home</a></li>
                                <!--drop-down-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Featured<b
                                                class="caret"></b></a>
                                    <ul class="dropdown-menu multi-column columns-2">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="<?php base_url(); ?>page/life-style">Life style</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/hostel-life">Hostel life</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/campass-life">Around My
                                                            university</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/school-politics">School
                                                            politics</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="<?php base_url(); ?>page/college-news">College Info</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/celebrity-gossip">My
                                                            celebrity</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/original-composition">Original
                                                            Composition</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/poem">Poem</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php base_url(); ?>page/movie-review">Movie
                                                            Reviews</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <!---//drop-down-->
                                <li><a href="<?php base_url(); ?>page/local-news">Local news</a></li>
                                <li><a href="<?php base_url(); ?>page/fashion-and-design">Fashion & style</a></li>
                                <li><a href="<?php base_url(); ?>page/campass-life">Campass vibes</a></li>
                                <li><a href="<?php base_url(); ?>page/entertainment">Entertainment</a></li>
                                <li><a href="<?php base_url(); ?>page/birthday">Birth-Days</a></li>
                                <li><a href="<?php base_url(); ?>pages/video">Videos</a></li>
                                <li><a href="<?php base_url(); ?>pages/about-us">About us</a></li>
                                <li><a href="<?php base_url(); ?>pages/contact-us">Contact us</a></li>
                                <li><a href="<?php base_url(); ?>pages/advertise">Advertise</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <!-----search-bar------>
            <div class="header_search">
                <button id="searchIcon"><i class="fa fa-search"></i></button>
                <div id="shide">
                    <div id="search-hide">
                        <form action="<?php base_url(); ?>search" method="post">
                            <input type="text" name="search" size="40" placeholder="Search this site ...">
                        </form>
                        <button class="remove"><span><i class="fa fa-times"></i></span></button>

                        <script type="text/javascript">
                            //private message search engine modal box
                            $(function () {

                                $('#search_text').keyup(function () {
                                    var a = $('#search_text').val();

                                    $.post('search.php', {"search": a},

                                        function (data) {

                                            $('#show_result').html(data);

                                        });

                                });
                            });

                        </script>
                    </div>
                </div>
            </div>
            <!--//search-bar-->
            <!--//top-header-->
            <!--start-header-->
            <div class="header_bottom">
                <div class="logo_area"><a class="logo" href="index">
                        <img src="<?php base_url(); ?>/images/tech1.jpg" alt="" width="70px"><b>C</b>ocis-news</a><h6>Be
                        inspired by innovation</h6></div>
                <img src="<?php base_url(); ?>/images/wifi.jpg" alt="" width="120px">
                <!--header-advert--
                <div class="top_addarea">

                    <a id="adChoicesLogo" href="https://www.quantcast.com/adchoices-pub?pub=Sq295CyOCSlFa6NGj5l0Jg"
                       target="_blank"
                       style="display:block; width:75px; height:16px; line-height: 16px; text-align:left; background: transparent url(https://aboutads.quantcast.com?icon=Sq295CyOCSlFa6NGj5l0Jg) right 0 no-repeat; color:#333; text-decoration:none; font-size:11px; font-family:arial,helvetica,sans-serif;">AdChoices</a>
                    <?php include("pages/header_ad.php"); ?>-->
                </div>
            </div>
            <!--//header-advert-->
            <!--developers-team-->
    </div>
        <?php
        sliding_title($dbcon);
        function sliding_title($dbcon)
        {
            $query = mysqli_query($dbcon, "select news_id, news_title, news_image from news where news_state = 2 order by edit_time desc limit 10 ") or die(mysqli_error($dbcon));
            echo "<div class=\"latest_newsarea\"> <span>Recent Updates</span>
			<ul id=\"ticker01\" class=\"news_sticker\">
			<li><a href=\"#\">Recent Updates</a></li>";
            while ($info = mysqli_fetch_array($query)) {
                echo "<li><a href='news?id=" . $info['news_id'] . "' target=\"_blank\">" . $info['news_title'] . "</a></li>\n";
            }
            echo "</ul>";
            echo "</div>";
        }

        ?>
        <!----//header----------->