<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tech Ninja-Ug | Tutorials</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords"
          content="A platform for learning new computer-tricks, download tutorials, android apps, softwares, free modem unlocking and advertisements">
    <!--style and bootstrap-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="assets/css/button.css">
    <link rel="icon" href="favicon.ico">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <!--push notifications-->
    <script src="https://clientcdn.pushengage.com/core/6570.js"></script>
    <script>
        _pe.subscribe();
    </script>
    <script type="text/javascript" src="assets/script/jquery-1.8.0.min.js"></script>
    <!--//push-->
    <!--disable right click-->
    <script language=JavaScript>
        //Disable right mouse click Script

        var message = "Don't dare do it again it will harm your device!!";

        function clickIE4() {
            if (event.button == 2) {
                alert(message);
                return false;
            }
        }

        function clickNS4(e) {
            if (document.layers || document.getElementById && !document.all) {
                if (e.which == 2 || e.which == 3) {
                    alert(message);
                    return false;
                }
            }
        }

        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
            document.onmousedown = clickNS4;
        }
        else if (document.all && !document.getElementById) {
            document.onmousedown = clickIE4;
        }

        document.oncontextmenu = new Function("alert(message);return false")

    </script>
</head>
<body>
<!--start-top-header-->
<!--<div id="preloader">
  <div id="status">&nbsp;</div>
</div>-->
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    <!--social-icons-->
    <div class="footer_bottom_right">
        <ul>
            <li><a class="tootlip" data-toggle="tooltip" data-placement="bottom" title="Twitter"
                   href="https://twitter.com/Clintonclincher" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a class="tootlip" data-toggle="tooltip" data-placement="bottom" title="Facebook"
                   href="https://www.facebook.com/Clintonclincher/" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li><a class="tootlip" data-toggle="tooltip" data-placement="bottom" title="Googel+"
                   href="https://plus.google.com/108536149586998980186" target="_blank"><i class="fa fa-google-plus"
                                                                                           target="_blank"></i></a></li>
            <li><a class="tootlip" data-toggle="tooltip" data-placement="bottom" title="Youtube"
                   href="https://www.youtube.com/channel/UCJ5IZFQKjneU1wQHV5LXvjQ" target="_blank"><i
                            class="fa fa-youtube"></i></a></li>
            <li><a class="tootlip" data-toggle="tooltip" data-placement="bottom" title="Pinterest"
                   href="https://www.pinterest.com/cclincher/"><i class="fa fa-pinterest" target="_blank"></i></a></li>
        </ul>
    </div>
    <!--//social-icons-->
    <!---counter---->

    <!---//counter--->
    <!-----clock-------->
    <SCRIPT
            LANGUAGE="JavaScript">

        var timerID = null;
        var timerRunning = false;

        function stopclock() {
            if (timerRunning)
                clearTimeout(timerID);
            timerRunning = false;
        }

        function showtime() {
            var now = new Date();
            var hours = now.getHours();
            var minutes = now.getMinutes();
            var seconds = now.getSeconds()

            var timeValue = "" + ((hours > 12) ? hours - 12 : hours)
            timeValue += ((minutes < 10) ? ":0" : ":") + minutes
            timeValue += ((seconds < 10) ? ":0" : ":") + seconds
            timeValue += (hours >= 12) ? " P.M." : " A.M."
            document.clock.face.value = timeValue;

// you could replace the above with this
// and have a clock on the status bar:
// window.status = timeValue;

            timerID = setTimeout("showtime()", 1000);
            timerRunning = true;
        }

        function startclock() {
// Make sure the clock is stopped
            stopclock();
            showtime();
        }
    </SCRIPT>
    <BODY onLoad="startclock(); timerONE=window.setTimeout"

          TEXT="ffffff">

    <CENTER>
        <form name="clock" onSubmit="0">
            <label>TIME</label>
            <input type="text" name="face" size=13 value=""></form>
    </CENTER>
    <!-----//clock-->
    <!---nav-bar-header-->
    <?php
    require_once("header.php");
    ?>
    <!----//header----------->
    <!------section---------->
    <section id="contentbody">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                <div class="row">
                    <div class="left_bar">
                        <!---side-nav-computer-tricks--->
                        <div class="single_leftbar">
                            <h2><span>Latest Pc Tricks</span></h2>
                            <?php include("pages/leftbar_tricks.php"); ?>
                        </div>
                        <!--//side-nav-computer-tricks-->
                        <!---left-advert--->
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Side Add</span></h2>
                            <div class="singleleft_inner">
                                <?php include("pages/leftad.php"); ?>
                            </div>
                        </div>
                        <!---//left-advert--->
                        <!---whatsapp-tricks--->
                        <div class="single_leftbar">
                            <h2><span>Whatsapp Tricks</span></h2>
                            <?php include("pages/whatsapp_tricks.php"); ?>
                        </div>
                        <!---//whatsapp-tricks--->
                    </div>
                </div>
            </div>
            <!-------//section----------->
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="middle_bar">
                        <h3><strong><code>WELCOME TO OUR PDF TUTORIAL DOWNLOAD PAGE.
                                    ENJOY MAXIMUM DOWNLOADS FOR FREE AND LEARN MORE...</code></strong></h3>
                        <!--start-categories-->
                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Html</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/html.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Css/Bootstrap</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/css.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Java/Jsp</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/java.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Php</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/php.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Javascript</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/javascript.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Android Development</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/android.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Python</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/python.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>C-Programming</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/cprog.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>C++ Programming</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/cplus.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Artficial Intelligent</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/ai.php"); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="single_category wow fadeInDown">
                            <div class="category_title"><a>Ios</a></div>
                            <div class="single_category_inner">
                                <ul class="catg3_snav catg5_nav">
                                    <?php include("doc_cat/ios.php"); ?>
                                </ul>
                            </div>
                        </div>
                        <!--//categories-->

                    </div>
                </div>
            </div>
            <!--start-trending-stories-->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="right_bar">
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Trending Stories</span></h2>
                            <?php include("pages/newssidebar.php"); ?>
                        </div>
                        <!---//trending-stories----->
                        <!--start facebook-box-->
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Follow Us Today</span></h2>
                            <div class="singleleft_inner">
                                <div id="fb-root"></div>
                                <script>(function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-page" data-href="https://www.facebook.com/Clintonclincher/"
                                     data-tabs="timeline" data-height="400" data-small-header="false"
                                     data-adapt-container-width="true" data-hide-cover="false"
                                     data-show-facepile="true"></div>
                                <!--//facebook-box-->
                            </div>
                        </div>
                        <!--right-top-advert-->
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Side Ad</span></h2>
                            <div class="singleleft_inner">
                                <?php include("pages/right_middlead.php"); ?>
                            </div>
                        </div>
                        <!--//right-top-advert-->
                        <!--software-categories-->
                        <div class="single_leftbar wow fadeInDown">
                            <ul class="nav nav-tabs custom-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                                          data-toggle="tab">Popular</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                           data-toggle="tab">Best</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                           data-toggle="tab">Recent</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                                        <li>
                                            <?php include("soft_cat/browser.php"); ?>
                                        </li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                                        <li>
                                            <?php include("soft_cat/antivirus.php"); ?>
                                        </li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                                        <li>
                                            <?php include("soft_cat/messaging.php"); ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--//software-categories-->
                        <!--right-bottom-ad-->
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Side Ad</span></h2>
                            <div class="singleleft_inner"><a href="#">
                                    <?php include("pages/right_bottomad.php"); ?>
                            </div>
                        </div>
                        <div class="single_leftbar wow fadeInDown">
                            <h2><span>Useful Links</span></h2>
                            <div class="singleleft_inner">
                                <ul class="link_nav">
                                    <?php include("pages/useful_links.php"); ?>
                                </ul>
                            </div>
                        </div>
                        <!--//right-bottom-ad-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--------//section------->
    <!-------footer----------->
    <?php
    require_once("footer.php");
    ?>
    <!-----//footer--------->
</div>
</div>
<!-----javascript---------->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.li-scroller.1.0.js"></script>
<script src="assets/js/custom.js"></script>
<!-----//javascript---------->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/584bfc68e2def07b70ac56f9/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>

<?php

include("includes/connect.php");

if (isset($_POST['submit'])) {
    $user_title = $_POST['name'];
    $user_date = date('y-m-d');
    $user_email = $_POST['email'];
    $user_message = $_POST['message'];

    if ($user_title == '' or $user_email == '' or
        $user_message == '') {

        echo "<script>alert('please fill out all fields!!')</script>";

        exit();
    } else {

        $insert_query = "Insert into contact (user_title,user_date,user_email,user_message) 
		 values('$user_title','$user_date','$user_email','$user_message')";

        $result = mysqli_query($dbcon, $insert_query);

        if ($result) {

            echo "<script>alert('Your Message has been sent successfully...')</script>";
        }
    }

}

?>