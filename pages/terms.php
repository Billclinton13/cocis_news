<?php
include_once("../header.php");
include_once("../leftbar.php");
?>
<!-------middle----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <h3><strong><code>PLEASE READ THESE TERMS AND CONDITIONS BEFORE YOU <br/> START USING OUR SITE.
                    </code></strong></h3>
            <div class="single_post_area">
                <div class="single_post_content">
                    <h3>
                        <mark>TERMS OF WEBSITE USE</mark>
                    </h3>
                    <br/>
                    <p>These terms of use tells you the terms of service on which you use
                        may make use of our
                        site <a href="//cocis.news">Cocis News</a></p>
                    <p>**Use of our site includes accessing, browsing to use, downloading or posting comments on the
                        site.</p>
                    <p>**Please read these terms of service carefully before you start to use our site,
                        as these will apply to your use of the site. We recommend that you print a copy of this page for
                        future reference.</p>
                    <p>**By using our site, you confirm that you accept these terms of use and that you agree to comply
                        with them.
                        If you do not agree to these terms of use, you must not use our site.</p>

                    <h4><span style="color:red">ACCESSING OUR SITE</span></h4>
                    <p>**Our site is made available free of charge and we accept no responsibility for any activity or
                        function of our site being unavailable.</p>
                    <p>**We do not guarantee that our site, or any content on it, will be free from errors or omissions,
                        will always be available or will be uninterrupted. </p>
                    <p>**Access to our site is permitted on a temporary basis.</p>
                    <p>**We may suspend, withdraw, discontinue or change all or any part of our site without notice.</p>
                    <p>**We will not be liable to you if for any reason our site is unavailable at any time or for any
                        period.</p>

                    <h4><span style="color:red">CHANGES TO THESE TERMS AND OUR SITE</span></h4>
                    <p>**We may revise these terms of use at any time by amending this page,
                        please check this page from time to time to take notice of any changes we made, as they are
                        binding on you.</p>
                    <p>**We may update our site from time to time, and may change the content at any time. However,
                        please note that any of the content on our site may be out of date at any given time,
                        and We are under no obligation to update it.</p>

                    <h4><span style="color:red">NO RELIANCE ON INFORMATION</span></h4>
                    <p>**The content on our site is provided for general information only. It is not intended to amount
                        to
                        advice on which you should rely. You must obtain professional or specialist advice before
                        taking,
                        or refraining from, any action on the basis of the content on our site.</p>

                    <h4><span style="color:red">PROHIBITED USES</span></h4>
                    <h4><code>You may use our site only for lawful purposes. You may not use our site:</code></h4>
                    <p>***In any way that breaches any applicable local, national or international law or
                        regulation.</p>
                    <p>***In any way that infringes any copyright, database right, trade mark or other intellectual
                        property rights of any third party.</p>
                    <p>***In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or
                        effect.</p>
                    <p>***For the purpose of harming or attempting to harm minors in any way.
                    <p>***To send, knowingly receive, upload,download, use or re-use any material which does not comply
                        with our content standards below.</p>
                    <p>***To transmit, or procure the sending of, any unsolicited or unauthorised advertising or
                        promotional
                        material or any other form of similar solicitation (spam).</p>
                    <blockquote>You also agree:

                        Not to access without authority, interfere with, damage or disrupt:
                        any part of our site;
                        any equipment or network on which our site is stored;
                        any software used in the provision of our site; or
                        any equipment or network or software owned or used by any third party.
                    </blockquote>
                    <h4><span style="color:red">VIRUSES</span></h4>
                    <P>***We do not guarantee that our site will be secure or free from bugs or viruses. </p>
                    <p>***You are responsible for configuring your information technology, computer programmes
                        and platform in order to access our site. You should use your own virus protection software
                        and ensure it is up to date.</p>
                    <p>***You must not misuse our site by knowingly introducing viruses, Trojans, worms, logic bombs or
                        other material which is malicious or technologically harmful.</p>
                    <p>***You must not attempt to gain
                        unauthorised access to our site, the server on which our site is stored or any server, computer
                        or database connected to our site.</p>
                    <p>***You must not attack our site via a denial-of-service attack
                        or a distributed denial-of service attack. </p>
                    <p>***By breaching this provision, you would commit a criminal
                        offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law
                        enforcement
                        authorities and We will co-operate with those authorities by disclosing your identity to them.
                        In the event
                        of such a breach, your right to use our site will cease immediately.</p>

                    <h4><span style="color:red">THIRD PARTY LINKS AND RESOURCES IN OUR SITE</span></h4>
                    <p>***Where our site contains links to other sites and resources provided by third parties,
                        these links are provided for your information only and we have no control over the contents of
                        those sites or resources.</p>

                    <h3><span style="color:#2AB2E3">Thanks Enjoy Using Our Site.....</span></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--start-trending-stories-->
<?php
include_once("../right-bar.php");
?>
</div>
</section>
<!--------//section------->
<!-------footer----------->
<?php
require_once("../footer.php");
?>
	