<?php
include_once("includes/connect.php");

$query = "select * from news where news_state = 2 order by news_read DESC LIMIT 0,5";

$run = mysqli_query($dbcon, $query);

while ($row = mysqli_fetch_array($run)) {
    $news_id = $row['news_id'];
    $title = $row['news_title'];
    $news_date = $row['news_date'];
    $image = $row['news_image'];
    ?>
    <div class="singleleft_inner">
        <ul class="catg3_snav ppost_nav wow fadeInDown">
            <li>
                <div class="media">
                    <a href="<?php base_url(); ?>news/<?php echo $news_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                       class="media-left img-responsive img-rounded">
                        <img src="<?php base_url(); ?>images/news/<?php echo $image; ?>"></a>
                    <div class="media-body">
                        <a href="<?php base_url(); ?>news/<?php echo $news_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                           class="recent_title">
                            <?php echo $title; ?>
                        </a><br/>
                        <i class="fa fa-clock-o"> <?php echo $news_date; ?></i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
<?php } ?>
              
	   
	
	