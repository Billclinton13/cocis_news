<?php
require_once("../header.php");
include_once("../leftbar.php");
?>
<!-------//section----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">
                <?php
                echo "<h2 style='text-align: center;'><code>ABOUT US</code></h2>";
                ?>
                <div class="single_post_content">
                    <h3><i><u>An Introduction to Cocis News:</u></i></h3><br/>
                    <p>As we all know Windows a user friendly, Linux an Open Source which are rapidly growing, our
                        objective is to promote Windows, Linux, and Mac
                        Systems & sharing knowledge with others.<br/><br/>

                    <h4><span style="color:red">TechNinja-Ug</span></h4>was started in September 15th 2016 as a Windows,
                    Linux, Mac community site by
                    <mark><a href="developer/developer" target="_blank">Clinton Nkesiga</a></mark>
                    for building a
                    one source site for all kind of Windows, Linux and Mac howto’s on the web. In less than one month
                    it becomes one of the rapidly growing community based website for all kind of Windows, Linux and Mac
                    howto’s, tricks,
                    news and reviews. <br/><br/>Our readers are mainly Windows and Linux users, system admins, IT
                    professionals and tech geeks.
                    In last 2 weeks, we’ve received many visitors and positive appreciations from our readers around the
                    globe.<br/><br/>

                    <a href="https://cocis.news">Cocis.news</a> has a excellent reputation in the search
                    engine ranking.
                    Our Vision is to reach Windows, Linux, Mac readers who are Newbie or a Savvy in this field.</p>

                    <h4 style="text-align: center"><i><u>Our Team</u></i></h4>
                    <div class="col-md-4 text-center">
                        <img <img src="../images/clinton.jpg" height="50px" width="200px" alt=""
                                  class="center-block img-circle img-responsive">
                    </div>
                    <div class="col-md-8">
                        <h5><i><b><span style="color:brown">MR: NKESIGA CLINTON</b></span></i></h5>
                        <p>I am the mind behind TechNinja-Ug</p>.
                        <p>I may use big words at times, but I’m a really simple guy who loves to simplify complex
                            stuff.
                            <br/>I completed my secondary education at Alliance Secondary School Ibanda in 2013 and
                            Joined
                            <a href="http://www.saipali.education" target="_blank">Saipali Institute of Technology &
                                Management</a> for diploma in Software Engineering
                            in 2015. Prior to becoming a professional developer.</p>
                        <a href="developer/developer" target="_blank">
                            <button class="style_btn">See More</button>
                        </a>
                    </div>
                </div>
                <div class="social_area wow fadeInLeft">
                    <h3 style="text-align: center">Stay Connected</h3>
                    <ul>
                        <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a>
                        </li>
                        <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li><a href="https://plus.google.com/u/1/communities/108139383578981516178"
                               target="_blank"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <li><a href="https://www.pinterest.com/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--start-trending-stories-->
<?php
include_once("../right-bar.php");
?>
<!--------//section------->
<!-------footer----------->
<?php
require_once("../footer.php");
?>
	