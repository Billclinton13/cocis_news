<section id="contentbody">
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
            <div class="row">
                <div class="left_bar">
                    <!---side-nav-computer-tricks--->
                    <div class="single_leftbar">
                        <h2><span>Random Updates</span></h2>
                        <?php include("pages/leftbar_tricks.php"); ?>
                    </div>
                    <!--//side-nav-computer-tricks-->
                    <!---left-advert--->
                    <div class="single_leftbar wow fadeInDown">
                        <h2><span>Side Ad</span></h2>
                        <div class="singleleft_inner">
                            <?php include("pages/leftad.php"); ?>
                        </div>
                    </div>
                    <!---//left-advert--->
                    <!---whatsapp-tricks--->
                    <div class="single_leftbar">
                        <h2><span>Notices</span></h2>
                        <?php include("pages/whatsapp_tricks.php"); ?>
                    </div>
                    <!---//whatsapp-tricks--->
                </div>
            </div>
        </div>