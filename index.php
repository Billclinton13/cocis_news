<?php
require_once("header.php");
?>
    <!------section---------->
<?php
include_once("leftbar.php");
?>
    <!-------//section----------->
    <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
        <div class="row">
            <div class="middle_bar">
                <!-----slider-images------->
                <div class="featured_sliderarea">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="item active"><a href="http://rbmafrica.com/quickbooks/" target="_blank"><img
                                            src="images/slider/Ads.jpg" alt=""></a>
                            </div>
                            <div class="item"><a href="http://cagex.co.nf" target="_blank"><img
                                            src="images/slider/ad-testimony.jpg" alt=""></a>
                            </div>
                            <div class="item"><img src="images/slider/fire.jpg" alt="">
                            </div>
                            <div class="item"><img src="images/slider/slide-android.png" alt="">
                            </div>
                            <div class="item"><img src="images/slider/webdesignBanner.jpg" alt="">
                            </div>
                        </div>
                        <a class="left left_slide" href="#myCarousel" role="button" data-slide="prev"> <span
                                    class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a><a
                                class="right right_slide" href="#myCarousel" role="button" data-slide="next"> <span
                                    class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a></div>
                </div>
                <!---//silder-images---------->
                <!--------Tricks----------->
                <div class="single_category wow fadeInDown">
                    <div class="category_title"><a href="">Celebrity Corners</a></div>
                    <div class="single_category_inner">
                        <ul class="catg_nav">
                            <?php include("pages/celebrity-corner.php"); ?>
                        </ul>
                    </div>
                </div>
                <div class="single_category wow fadeInDown">
                    <div class="category_title"><a href="#">Technology & innovations</a></div>
                    <div class="single_category_inner">
                        <ul class="catg3_snav catg5_nav">
                            <?php include("pages/net_tricks.php"); ?>
                        </ul>
                    </div>
                </div>
                <div class="category_three_fourarea  wow fadeInDown">
                    <div class="category_three">
                        <div class="single_category">
                            <div class="category_title"><a href="">Birth Days</a></div>
                            <div class="single_category_inner">
                                <ul class="catg_nav catg_nav3">
                                    <?php include("pages/birthday.php"); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="category_four wow fadeInDown">
                        <div class="single_category">
                            <div class="category_title"><a href="">Events and Happenings</a></div>
                            <div class="single_category_inner">
                                <ul class="catg_nav catg_nav3">
                                    <?php include("pages/events.php"); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!---//Tricks----------->
                <!-------apps------------>
                <div class="single_category wow fadeInDown">
                    <div class="category_title"><a href="#">Local News and Updates</a></div>
                    <div class="single_category_inner">
                        <ul class="catg3_snav catg5_nav">
                            <?php include("pages/android_apps.php"); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--start-trending-stories-->
<?php
include_once("right-bar.php");
?>
    <!--------//section------->
<?php
require_once("footer.php");
?>