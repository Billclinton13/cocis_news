<title>Cocis News | Contact us</title>
<?php
include_once("../header.php");
require_once("../leftbar.php");
?>
<!--middle-->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">
                <?php
                echo "<h2 style='text-align: center'><code>CONTACT US</code></h2>";
                ?>
                <div class="single_post_content">
                    <section id="contentSection">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="left_content">
                                    <div class="contact_area">
                                        <h4>Share with us your thoughts we love to help you...</h4>
                                        <ul>
                                            <li>Telephone :+256778607383 / +256705098174</li>
                                            <li>WhatsApp :+256705098174 / +256758557567</li>
                                            <li>Email :<a href="mailto:clinton@cocis.news"> clinton@cocis.news</a></li>
                                            <li>Email :<a href="mailto:ashiraff@cocis.news"> ashiraff@cocis.news</a>
                                            </li>
                                        </ul>
                                        <form action="contact.php" id="disableInput" method="post" class="contact_form">
                                            <input class="form-control" id="disableInput" name="name" type="text"
                                                   placeholder="Name*" disabled=""/>
                                            <input class="form-control" id="disableInput" name="email" type="email"
                                                   placeholder="Email*" disabled=""/>
                                            <textarea class="form-control" id="disableInput" name="message" cols="30"
                                                      rows="10" placeholder="Message*" disabled=""></textarea><br>
                                            <input class="send_btn" id="disableInput" name="submit" type="submit"
                                                   value="Send" disabled=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </section>
                </div>
                <div class="social_area wow fadeInLeft">
                    <center><h3>Stay Connected</h3></center>
                    <ul>
                        <li><a href="https://twitter.com/Clintonclincher" target="_blank"><span
                                        class="fa fa-facebook"></span></a></li>
                        <li><a href="https://www.facebook.com/Clintonclincher/" target="_blank"><span
                                        class="fa fa-twitter"></span></a></li>
                        <li><a href="https://plus.google.com/108536149586998980186" target="_blank"><span
                                        class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/home?trk=nav_responsive_tab_home" target="_blank"><span
                                        class="fa fa-linkedin"></span></a></li>
                        <li><a href="https://www.pinterest.com/cclincher/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--start-trending-stories-->
<?php
include_once("../right-bar.php");
?>
<!--------//section------->
<!--------footer------->
<?php
require_once("../footer.php");
?>
<!-----//footer--------->
 