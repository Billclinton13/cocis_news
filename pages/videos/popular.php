<?php
include("../includes/connect.php");
function videos_by_category($cat, $dbcon)
{
    $query = "select * from videos where cat_id = '$cat' order by 1 DESC LIMIT 0,10";
    $run = mysqli_query($dbcon, $query);
    while ($row = mysqli_fetch_array($run)) {
        $id = $row['video_id'];
        $name = $row['name'];
        $title = $row['title'];
        $size = round(($row['size'] / pow(1024, 2)), 2) . "mb";
        $vid_image = $row['vid_image'];
        $url = $row['url'];
        html($url, $title, $size, $vid_image);
    }
}

function html($url, $title, $size, $vid_image)
{
    ?>
    <div class="col-xs-5 col-sm-3">
        <br/>
        <div class="ender-wow"></div>
        <div class="well well-sm">
            <video id="my-video" class="video-js"
                   allowfullscreen="" controls preload="auto" width="250" height="200"
                   poster="../videos/images/<?php echo $vid_image; ?>" data-setup="{}">
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/mp4'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/webm'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/mpg'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/3gp'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/MP4'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/m4a'>
                <source src="<?php base_url(); ?>videos/vid/<?php echo $url; ?>" type='video/flv'>
            </video>
            <h5><?php echo $title; ?></h5>
            <p><?php echo $size; ?></p>
        </div>
    </div>
    <?php
}

?>