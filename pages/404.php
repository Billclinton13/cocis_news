<!DOCTYPE html>
<html>
<head>
    <title>Error Page</title>
</head>
<!-----//clock-->
<!---nav-bar-header-->
<?php
require_once("../header.php");
?>
<!----//header----------->
<section id="errorpage_body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="errorpage_area">
                <div class="error-title"><span>404</span></div>
                <div class="error_content">
                    <p><i class="fa fa-hand-o-right "></i> Sorry, the page you were looking for in this site is not
                        available at the moment.<br> You can visit us next time</p>
                    <a href="../index.php">Home</a></div>
            </div>
        </div>
    </div>
</section>
<!--//section-->
<!-------footer----------->
<?php
require_once("../footer.php");
?>
<!-----//footer--------->
</div>
</div>
<!-----javascript---------->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/slick.min.js"></script>
<script src="../assets/js/jquery.li-scroller.1.0.js"></script>
<script src="../assets/js/custom.js"></script>
</body>
</html>

<?php

include("includes/connect.php");

if (isset($_POST['submit'])) {
    $user_title = $_POST['name'];
    $user_date = date('y-m-d');
    $user_email = $_POST['email'];
    $user_message = $_POST['message'];

    if ($user_title == '' or $user_email == '' or
        $user_message == '') {

        echo "<script>alert('please fill out all fields!!')</script>";

        exit();
    } else {

        $insert_query = "Insert into contact (user_title,user_date,user_email,user_message) 
		 values('$user_title','$user_date','$user_email','$user_message')";

        $result = mysqli_query($dbcon, $insert_query);

        if ($result) {

            echo "<script>alert('Your Message has been sent successfully...')</script>";
        }
    }

}

?>