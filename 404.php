<!DOCTYPE html>
<html>
<head>
    <title>Error Page</title>
</head>
<!-----//clock-->
<!---nav-bar-header-->
<?php
require_once("header.php");
?>
<!----//header----------->
<section id="errorpage_body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="errorpage_area">
                <div class="error-title"><span>404</span></div>
                <div class="error_content">
                    <p><i class="fa fa-hand-o-right "></i> Sorry, the page you were looking for in this site is not
                        available at the moment.<br> You can visit us next time</p>
                    <a href="../index.php">Home</a></div>
            </div>
        </div>
    </div>
</section>
<!--//section-->
<!-------footer----------->
<?php
require_once("footer.php");
?>
<!-----//footer--------->