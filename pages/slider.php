<?php
include("includes/connect.php");

$query = "select * from slider order by 1 DESC LIMIT 0,5";

$run = mysqli_query($dbcon, $query);
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner" role="listbox">


        <?php
        while ($row = mysqli_fetch_array($run)){

        $slider_id = $row['slider_id'];
        $title = $row['slider_title'];
        $image = $row['slider_image'];


        ?>

        <div class="item active">
            <img src="images/slider/<?php echo $image; ?>" alt="" width="800px" height="300px">
            <div class="carousel-caption">
                <h1><a href="<?php echo $slider_id; ?>"></a></h1>
            </div>

        </div>
    </div>
    <a class="left left_slide" href="#myCarousel" role="button" data-slide="prev"> <span
                class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a> <a class="right right_slide"
                                                                                            href="#myCarousel"
                                                                                            role="button"
                                                                                            data-slide="next"> <span
                class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a></div>


<?php } ?>
             
	   
	
	