<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="row">
        <div class="right_bar">
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Trending Stories</span></h2>
                <?php include("pages/newssidebar.php"); ?>
            </div>
            <!---//trending-stories----->
            <!--start facebook-box-->
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Follow Us Today</span></h2>
                <div class="singleleft_inner">
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/cocisnews/" data-tabs="timeline"
                         data-height="400" data-small-header="false" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true"></div>
                    <!--//facebook-box-->
                </div>
            </div>
            <!--right-top-advert-->
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Side Ad</span></h2>
                <div class="singleleft_inner">
                    <?php include("pages/right_middlead.php"); ?>
                </div>
            </div>
            <!--//right-top-advert-->
            <!--software-categories-->
            <div class="single_leftbar wow fadeInDown">
                <ul class="nav nav-tabs custom-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                              data-toggle="tab">Popular</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                               data-toggle="tab">Best</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Recent</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <?php include_once("soft_cat/browser.php"); ?>
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <?php side_news($dbcon, 1); ?>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <?php side_news($dbcon, 2); ?>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <?php side_news($dbcon, 3); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--//software-categories-->
            <!--right-bottom-ad-->
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Side Ad</span></h2>
                <div class="singleleft_inner"><a href="#">
                        <?php include("pages/right_bottomad.php"); ?>
                </div>
            </div>
            <div class="single_leftbar wow fadeInDown">
                <h4>Useful Links</h4>
                <div class="singleleft_inner">
                    <ul class="link_nav">
                        <?php include("pages/useful_links.php"); ?>
                    </ul>
                </div>
            </div>
            <!--//right-bottom-ad-->
        </div>
    </div>
</div>