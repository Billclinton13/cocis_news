<?php
include("includes/connect.php");

$query = "select * from news where cat_id = 9 and news_state = 2 order by edit_time desc LIMIT 0, 5";

$run = mysqli_query($dbcon, $query);

while ($row = mysqli_fetch_array($run)) {

    $trick_id = $row['news_id'];
    $title = $row['news_title'];
    $image = $row['news_image'];
    $trick_date = $row['news_date'];
    ?>
    <div class="singleleft_inner">
        <ul class="catg3_snav ppost_nav wow fadeInDown">
            <li>
                <div class="media">
                    <a href="<?php base_url(); ?>news/=<?php echo $trick_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                       class="media-left img-responsive img-rounded">
                        <img src="<?php base_url(); ?>images/news/<?php echo $image; ?>"></a>
                    <div class="media-body">
                        <a href="<?php base_url(); ?>news/=<?php echo $trick_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                           class="recent_title">
                            <?php echo $title; ?>
                        </a><br/>
                        <i class="fa fa-clock-o"> <?php echo $trick_date; ?></i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
<?php }
?>	
             
	   
	
	