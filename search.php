<?php
include_once("header.php");
?>
<!----//header----------->
<!------section---------->
<?php
include("leftbar.php");
?>
<!------middle----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">
                <ol class="breadcrumb">
                    <li><a href="index"><i class="fa fa-home"></i>Home<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="search">Search<i class="fa fa-angle-right"></i></a></li>
                    <li class="active">Results</li>
                </ol>

                <?php
                echo "<h3><code>Your Search Result is here....</code></h3>";
                $search_id = $_POST['search'];
                //search from news
                $search_query = "select * from news where news_keywords like '%$search_id%' and news_state = 2";
                $count = "SELECT COUNT(*) FROM news WHERE news_keywords like '%$search_id%'";
                $run = mysqli_query($dbcon, $search_query);//here run the sql query.
                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $news_id = $search_row['news_id'];
                    $news_title = $search_row['news_title'];
                    $news_date = $search_row['news_date'];
                    $news_image = $search_row['news_image'];
                    ?>
                    <!--search from news-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="news78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $news_id; ?>"
                                       class="media-left">
                                        <img src="images/news/<?php echo $news_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="news78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $news_id; ?>"
                                           class="recent_title">
                                            <?php echo $news_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $news_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php }
                //search from latest_tricks
                $search_query = "select * from latest_tricks where trick_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM latest_tricks WHERE trick_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $trick_id = $search_row['trick_id'];
                    $trick_title = $search_row['trick_title'];
                    $trick_date = $search_row['trick_date'];
                    $trick_author = $search_row['trick_author'];
                    $trick_image = $search_row['trick_image'];

                    ?>

                    <!--search from latest_tricks-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="tricks7575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu36?id=<?php echo $trick_id; ?>"
                                       class="media-left">
                                        <img src="images/latesttricks/<?php echo $trick_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="tricks7575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu36?id=<?php echo $trick_id; ?>"
                                           class="recent_title">
                                            <?php echo $trick_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $trick_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];
                //search from hacking
                $search_query = "select * from hacking where hack_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM hacking WHERE hack_keywords like '%$search_id%'";


                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $hack_id = $search_row['hack_id'];
                    $hack_title = $search_row['hack_title'];
                    $hack_date = $search_row['hack_date'];
                    $hack_author = $search_row['hack_author'];
                    $hack_image = $search_row['hack_image'];

                    ?>
                    <!--search from hacking-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="hack75Ut$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuii?id=<?php echo $hack_id; ?>"
                                       class="media-left">
                                        <img src="images/hacking/<?php echo $hack_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="hack75Ut$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuii?id=<?php echo $hack_id; ?>"
                                           class="recent_title">
                                            <?php echo $hack_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $hack_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];

                //search from internet
                $search_query = "select * from internet where net_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM internet WHERE net_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $net_id = $search_row['net_id'];
                    $net_title = $search_row['net_title'];
                    $net_date = $search_row['net_date'];
                    $net_author = $search_row['net_author'];
                    $net_image = $search_row['net_image'];
                    ?>
                    <!--search from internet-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="net78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $net_id; ?>"
                                       class="media-left">
                                        <img src="images/internet/<?php echo $net_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="net78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $net_id; ?>"
                                           class="recent_title">
                                            <?php echo $net_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $net_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];
                //search from linux
                $search_query = "select * from linuxos where linux_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM linuxos WHERE linux_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $linux_id = $search_row['linux_id'];
                    $linux_title = $search_row['linux_title'];
                    $linux_date = $search_row['linux_date'];
                    $linux_author = $search_row['linux_author'];
                    $linux_image = $search_row['linux_image'];
                    ?>

                    <!--search from linux-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="linux7575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu34?id=<?php echo $linux_id; ?>"
                                       class="media-left">
                                        <img src="images/linux/<?php echo $linux_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="linux7575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu34?id=<?php echo $linux_id; ?>"
                                           class="recent_title">
                                            <?php echo $linux_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $linux_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];

                //search from macos
                $search_query = "select * from macos where mac_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM macos WHERE mac_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $mac_id = $search_row['mac_id'];
                    $mac_title = $search_row['mac_title'];
                    $mac_date = $search_row['mac_date'];
                    $mac_author = $search_row['mac_author'];
                    $mac_image = $search_row['mac_image'];
                    ?>
                    <!--search from mac-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="mac78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu?id=<?php echo $mac_id; ?>"
                                       class="media-left">
                                        <img src="images/mac/<?php echo $mac_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="mac78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYu?id=<?php echo $mac_id; ?>"
                                           class="recent_title">
                                            <?php echo $mac_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $mac_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];

                //search from whatsapp
                $search_query = "select * from whatsapp where whats_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM whatsapp WHERE whats_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {

                    $whats_id = $search_row['whats_id'];
                    $whats_title = $search_row['whats_title'];
                    $whats_date = $search_row['whats_date'];
                    $whats_author = $search_row['whats_author'];
                    $whats_image = $search_row['whats_image'];
                    ?>
                    <!--search from whatsapp-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="whatsapp4575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRY?id=<?php echo $whats_id; ?>"
                                       class="media-left">
                                        <img src="images/whatsapp/<?php echo $whats_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="whatsapp4575t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRY?id=<?php echo $whats_id; ?>"
                                           class="recent_title">
                                            <?php echo $whats_title; ?>
                                        </a><br/><br/>
                                        <a class><i class="fa fa-clock"></i><?php echo $whats_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];
                //search from android
                $search_query = "select * from android where app_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM news WHERE app_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {
                    $app_id = $search_row['app_id'];
                    $app_title = $search_row['app_title'];
                    $app_date = $search_row['app_date'];
                    $app_image = $search_row['app_image'];
                    $app_size = $search_row['app_size'];
                    ?>

                    <!--search from android-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="latest-android-apps-12635728392$20$*738-746$*2073QXY-GDHSA&gs&&SS?id=<?php echo $app_id; ?>"
                                       class="media-left">
                                        <img src="images/apps/<?php echo $app_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="latest-android-apps-12635728392$20$*738-746$*2073QXY-GDHSA&gs&&SS?id=<?php echo $app_id; ?>"
                                           class="recent_title">
                                            <?php echo $app_title; ?>
                                        </a><br/><br/>
                                        <a class="author_name"><i class="fa fa-user"></i><?php echo $app_size; ?></a>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $app_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];
                //search from software
                $search_query = "select * from software where soft_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM software WHERE soft_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {
                    $soft_id = $search_row['soft_id'];
                    $soft_title = $search_row['soft_title'];
                    $soft_date = $search_row['soft_date'];
                    $soft_image = $search_row['soft_image'];
                    $soft_size = $search_row['soft_size'];
                    ?>
                    <!--search from software-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="software_page78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $soft_id; ?>"
                                       class="media-left">
                                        <img src="images/software/<?php echo $soft_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="software_page78975t$*dDH-oPOSu-h&&&shhsjkfTSll-SSl$$uDGS-H84&&7d6&$e&36-37$$44592*20738-746$*2073QXY-GS*&&RSA-&UTYRYuh?id=<?php echo $soft_id; ?>"
                                           class="recent_title">
                                            <?php echo $soft_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $soft_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>

                <?php

                $search_id = $_POST['search'];
                //search from documents
                $search_query = "select * from documents where doc_keywords like '%$search_id%'";
                $count = "SELECT COUNT(*) FROM documents WHERE doc_keywords like '%$search_id%'";

                $run = mysqli_query($dbcon, $search_query);//here run the sql query.

                while ($search_row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {
                    $doc_id = $search_row['doc_id'];
                    $doc_title = $search_row['doc_title'];
                    $doc_date = $search_row['doc_date'];
                    $doc_image = $search_row['doc_image'];
                    $doc_size = $search_row['doc_size'];


                    ?>
                    <!--search from documents-->
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown">
                            <li>
                                <div class="media">
                                    <a href="latest-pdf-tutorials-GF$$YH-7847-6&$e&36-37$$92*20738-GSFDRSA-&UTYRYYyx?id=<?php echo $doc_id; ?>"
                                       class="media-left">
                                        <img src="images/docs/<?php echo $doc_image; ?>"
                                             class="img-responsive img-circle"></a>
                                    <div class="media-body">
                                        <a href="latest-pdf-tutorials-GF$$YH-7847-6&$e&36-37$$92*20738-GSFDRSA-&UTYRYYyx?id=<?php echo $doc_id; ?>"
                                           class="recent_title">
                                            <?php echo $doc_title; ?>
                                        </a><br/><br/>
                                        <a class="post_date"><i class="fa fa-clock-o"></i><?php echo $doc_date; ?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } ?>
                <!--end of search-->
                <div class="social_area wow fadeInLeft">
                    <ul>
                        <li><a href="https://twitter.com/Clintonclincher" target="_blank"><span
                                        class="fa fa-facebook"></span></a></li>
                        <li><a href="https://www.facebook.com/Clintonclincher/" target="_blank"><span
                                        class="fa fa-twitter"></span></a></li>
                        <li><a href="https://plus.google.com/108536149586998980186" target="_blank"><span
                                        class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/home?trk=nav_responsive_tab_home" target="_blank"><span
                                        class="fa fa-linkedin"></span></a></li>
                        <li><a href="https://www.pinterest.com/cclincher/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
                <div class="related_post">
                    <h2 class="wow fadeInLeftBig">You may also like <i class="fa fa-thumbs-o-up"></i></h2>
                    <ul class="recentpost_nav relatedpost_nav wow fadeInDown animated">
                        <?php include("pages/newssidebar.php"); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--start-trending-stories-->
<?php
include_once("right-bar.php");
?>
</div>
</section>
<!--------//section------->
<!-------footer----------->
<?php
require_once("footer.php");
?>
<!-----//footer--------->