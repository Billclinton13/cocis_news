<title>Cocis News | Advertise with us</title>
<?php
include_once("../header.php");
require_once("../leftbar.php");
?>
<!------section---------->
<!-------middle----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">

                <?php
                echo "<h2 style='text-align: center'><code>ADVERTISE WITH US</code></h2>";
                ?>
                <div class="single_post_content">
                    <h4><span style="color:red">Welcome to Cocis News Advertising page.</span></h4><br/>

                    <p> In this Media-Kit page you will find all the details that you will need to showcase your product
                        to millions of Cocis News readers. If this is your first visit to this community page, I would
                        suggest you
                        to go through our About us page to know more about this awesome Cocis News community and Geeks
                        behind this site.</p>
                    <p>
                        <mark>Grow your business by advertising on Cocis News. Showcase your services on Cocis News.
                            Connect with our rapidly-growing audience.
                            Reach thousands of viewers from around the world.
                        </mark>
                    </p>
                    <br/>
                    <h4><span style="color:red">To advertise on our site:</span></h4>
                    <p>Contact us for prices</p>
                    <ul>
                        <li>Telephone :+256705098174/+256778607383</li>
                        <li>WhatsApp :+256705098174/+256758557567</li>
                        <li>Email :<a href="mailto:advertise@cocis.news"> advertise@cocis.news</a></li>
                        <li>Email :<a href="mailto:ashiraff@cocis.news"> ashiraff@cocis.news</a></li>
                        <li>Email :<a href="mailto:clinton@cocis.news"> clinton@cocis.news</a></li>
                    </ul>
                </div>
                <div class="social_area wow fadeInLeft">
                    <h3 style="text-align: center">Stay Connected</h3>
                    <ul>
                        <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a>
                        </li>
                        <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li><a href="https://plus.google.com/u/1/communities/108139383578981516178"
                               target="_blank"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <li><a href="https://www.pinterest.com/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--start-trending-stories-->
<?php
include_once("../right-bar.php");
?>

<!--------//section------->
<!-------footer----------->
<?php
require_once("../footer.php");
?>
<!-----//footer--------->