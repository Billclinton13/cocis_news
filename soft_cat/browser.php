<?php
function side_news($dbcon, $switch)
{
    switch ($switch) {
        case 1:
            $query = "select * from news where news_state = 2 order by news_read desc LIMIT 0,5";
            break;
        case 2:
            $query = "select * from news where news_state = 2 order by news_read desc LIMIT 0,5";
            break;
        case 3:
            $query = "select * from news where news_state = 2 order by edit_time desc LIMIT 0,5";
            break;
    }
    $run = mysqli_query($dbcon, $query) or die(mysqli_error($dbcon));
    while ($row = mysqli_fetch_array($run)) {
        $soft_id = $row['news_id'];
        $title = $row['news_title'];
        $cat = $row['cat_id'];
        $soft_date = $row['news_date'];
        $news_auhor = $row['news_author'];
        $image = $row['news_image'];
        $desciption = $row['short_desc'];
        ?>
        <li>
            <div class="media"><a
                        href="<?php base_url(); ?>news/<?php echo $soft_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                        class="media-left">
                    <img alt="" src="<?php base_url(); ?>images/news/<?php echo $image; ?>"></a>
                <div class="media-body">
                    <a href="<?php base_url(); ?>news/<?php echo $soft_id; ?>/<?php echo str_replace(" ", "-", $title); ?>"
                       class="catg_title"><?php echo $title; ?></a>
                    <div class="sing_commentbox">
                        <p>
                            <i class="fa fa-comments"></i><?php echo $desciption; ?>
                            <br>
                            <small>
                                <i class="fa fa-clock"><?php echo $soft_date; ?> </i> <i
                                        class="fa fa-user"> <?php echo $news_auhor; ?> </i>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </li>
        <?php
    }
}

?>
             
	   
	
	