<?php
include_once("includes/connect.php");
$url = trim($_GET['id']);
$url = mysqli_real_escape_string($dbcon, $url);
$query = mysqli_query($dbcon, "select * from categories where url_link = '$url' ") or die(mysqli_error($con));
if (mysqli_num_rows($query) < 1) {
    header("location:404");
    exit;
}
$info = mysqli_fetch_array($query);
$page = 1;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>COCIS NEWS | <?php echo $info['cat_title']; ?> </title>
    <meta name="keywords" content="<?php echo $info['keywords']; ?>"/>
    <meta name="description" content="<?php echo $info['description']; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo $info['cat_title']; ?>"/>
    <meta property="fb:app_id" content="1303916186392315"/>
    <meta property="og:description" content="<?php $info['description']; ?>"/>
    <meta property="og:image" content="//cocis.news/tech1.jpg">
</head>
<?php
require_once("header.php");
?>
<!----//header----------->
<!------section---------->
<?php
include_once("leftbar.php");
?>
<!-------middle----------->
<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
    <div class="row">
        <div class="middle_bar">
            <div class="single_post_area">
                <ol class="breadcrumb">
                    <li><a href="<?php base_url(); ?>index"><i class="fa fa-home"></i>Home<i
                                    class="fa fa-angle-right"></i></a></li>
                    <li>
                        <a href="<?php base_url(); ?>/page/<?php echo $info['url_link']; ?>"><?php echo $info['cat_title']; ?>
                            <i class="fa fa-angle-right"></i></a></li>
                    <li class="active">Categories</li>
                </ol>

                <?php

                $cat_id = $info['cat_id'];

                $select_query = "select * from news where cat_id = '$cat_id'  and news_state = 2 ";

                $run = mysqli_query($dbcon, $select_query);//here run the sql query.
                if (mysqli_num_rows($run) < 1) {
                    echo "<div class='alert alert-warning'> We could not get you results from <b>" . $info['cat_title'] . "</b> at the moment </div>";
                }
                while ($row = mysqli_fetch_array($run))//while look to fetch the result and store in a array $row.
                {
                    $news_id = $row['news_id'];
                    $news_title = $row['news_title'];
                    $new_date = $row['news_date'];
                    $news_author = $row['news_author'];
                    $news_image = $row['news_image'];
                    $news_content = $row['short_desc'];
                    ?>

                    <div class="single_category wow fadeInDown">
                        <div class="category_title"><a class><?php echo $news_title; ?></a></div>
                        <div class="single_category_inner">
                            <div class="classified">
                                <ul class="catg3_snav catg5_nav">
                                    <li>
                                        <div class="media"><a class="media-left">
                                                <img alt=""
                                                     src="<?php base_url(); ?>images/news/<?php echo $news_image; ?>"></a>
                                            <div class="media-body">
                                                <a href="<?php base_url(); ?>/news/<?php echo $news_id;
                                                echo "/";
                                                echo str_replace(" ", "-", $news_title); ?>"><?php echo $info['cat_title']; ?></a>
                                                <div class="sing_commentbox">
                                                    <p><i class="fa fa-comments"></i><?php echo $news_content; ?></a>
                                                    </p>
                                                    <br/><br/>
                                                    <a href="<?php base_url(); ?>/news/<?php echo $news_id;
                                                    echo "/";
                                                    echo str_replace(" ", "-", $news_title); ?>">
                                                        <button class="style_btn">Read</button>
                                                    </a>

                                                    <style>
                                                        .style_btn {
                                                            width: 150px;
                                                            color: #333;
                                                            height: 40px;
                                                            font-weight: bold;
                                                            font-family: cursive;
                                                            background: #1AB7EA;
                                                            border: 1px solid #1AB7EA;
                                                            border-radius: 5px;
                                                            padding: 11px;
                                                            font-size: 15px;
                                                            cursor: pointer;
                                                            box-shadow: 5px 5px 3px silver;

                                                        }

                                                        .style_btn:hover {
                                                            width: 150px;
                                                            height: 40px;
                                                            color: azure;
                                                            font-weight: bold;
                                                            font-family: cursive;
                                                            background: #333;
                                                            border: none;
                                                            border-radius: 8px;
                                                            padding: 11px;
                                                            font-size: 15px;
                                                            cursor: pointer;
                                                            box-shadow: 5px 10px 3px azure;
                                                        }</style>
                                                    <br/>
                                                    <br/>
                                                    <p align="justify"
                                                       class="content"><?php echo "<i class='fa fa-user'> </i>";
                                                        echo $news_author;
                                                        echo "<i class='fa fa-clock'> </i>";
                                                        echo $new_date; ?></p></div>
                                            </div>
                                        </div>
                            </div>
                            </li>
                        </div>
                    </div>
                    <?php
                } ?>

                <div class="post_footer">
                    <div id="disqus_thread"></div>
                    <script>
                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                        /*
                        var disqus_config = function () {
                        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };
                        */
                        (function () { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://cocis-news.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript style="visibility: hidden">Please enable JavaScript to view the <a
                                href="https://disqus.com/?ref_noscript">comments
                            powered by Disqus.</a></noscript>

                </div>
                <div class="social_area wow fadeInLeft">
                    <ul>
                        <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a>
                        </li>
                        <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li><a href="https://plus.google.com/u/1/communities/108139383578981516178"
                               target="_blank"><span class="fa fa-google-plus"></span></a></li>
                        <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <li><a href="https://www.pinterest.com/" target="_blank"><span
                                        class="fa fa-pinterest"></span></a></li>
                    </ul>
                </div>
                <div class="related_post">
                    <h2 class="wow fadeInLeftBig">You may also like <i class="fa fa-thumbs-o-up"></i></h2>
                    <ul class="recentpost_nav relatedpost_nav wow fadeInDown animated">
                        <?php include("pages/celebrity-corner.php"); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--start-trending-stories-->
<?php
include_once("right-bar.php");
?>

<!--------//section------->
<!-------footer----------->
<?php
require_once("footer.php");
?>
<!-----//footer--------->
 