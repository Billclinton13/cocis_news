<!-------footer----------->
<footer id="footer">
    <div class="footer_top">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <!--news-letter------>
            <div class="single_footer_top wow fadeInLeft">
                <h2>Never Miss Any Update</h2>
                <div class="subscribe_area">
                    <?php if (@$msg_sucess == "") : ?>
                    <p><span style="color:yellowgreen">
			  "Subscribe here to get our daily updates directly in your inbox"</span></p>
                    <form action="<?php base_url(); ?>pages/newsletter" method="post">
                        <div class="subscribe_mail">
                            <input class="form-control" name="email" type="email" placeholder="Email Address">
                            <i class="fa fa-envelope"></i></div>
                        <?php endif; ?>
                        <div class="<?= (@$msg_sucess == "") ? 'error' : 'green'; ?>"
                             id="logerror"><?php echo @$msg; ?><?php echo @$msg_sucess; ?></div>
                        <?php if (@$msg_sucess == "") : ?>
                            <input class="submit_btn" name="form_submit" type="submit" value="Submit">
                        <?php endif; ?>
                    </form>
                </div>
            </div>
            <br/>
            <!----//news-letter------>
            <!-----admin------>
            <div class="media"><img src="<?php base_url(); ?>/images/tech1.jpg" height="100px" width="100px" alt=""
                                    class="center-block img-circle img-responsive">
                <div class="profile" style="text-align: center"><h5><span style="color:#1AB7EA">Cocis News</span></h5>
                    <h6><span style="color:yellowgreen">Entertainment and Information</span><br/><br/>
                        <a href="<?php base_url(); ?>pages/about-us">
                            <button class="style_btn">Our Profile</button>
                        </a>
                </div>
            </div>
            <br/>
        </div>
        <!----//admin------->
        <!-----site-links---->
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="single_footer_top wow fadeInLeft">
                <h2>Site Links</h2>
                <ul class="catg3_snav ppost_nav">
                    <li>
                        <div class="media">
                            <div class="media-body"><a class="catg_title" href="<?php base_url(); ?>pages/advertise">
                                    Advertise With Us</a></div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-body"><a class="catg_title" href="<?php base_url(); ?>pages/donate">
                                    Donate to Cocis News</a></div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-body"><a class="catg_title" href="<?php base_url(); ?>pages/terms"> Terms
                                    of Service</a></div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-body"><a class="catg_title" href="<?php base_url(); ?>pages/about-us">
                                    About</a></div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-body"><a class="catg_title" href="<?php base_url(); ?>pages/contact-us">
                                    Contact</a></div>
                        </div>
                    </li>
                </ul>
                <div class="media">
                    <a class="twitter-follow-button"
                       href="https://twitter.com/NewsCocis" data-size="large">
                        Follow @Cocis News</a>
                </div>
            </div>
        </div>

        <!-----//site-links------>
        <!-----Most-downloaded---->
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="single_footer_top wow fadeInRight">
                <h2>Campass life</h2>
                <ul class="footer_labels">
                    <?php include("pages/most_downloads.php"); ?>
                </ul>
            </div>
        </div>
        <!-----//Most-downloaded---->
        <!-----Contact-us-form----->
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="single_footer_top wow fadeInRight">
                <h2>Contact Us</h2>
                <form action="email" method="post" class="contact_form">
                    <label>Name*</label>
                    <input class="form-control" id="disabledInput" name="name" type="text" placeholder="Name"
                           required=""/>
                    <label>Email*</label>
                    <input class="form-control" id="disabledInput" name="email" type="email" placeholder="Email"
                           required=""/>
                    <label>Message*</label>
                    <textarea class="form-control" id="disabledInput" name="content" cols="30" rows="10"
                              placeholder="Message" required=""/></textarea>
                    <input class="send_btn" id="disabledInput" name="submit" type="submit" value="Send"/>
                </form>
            </div>
        </div>
    </div>
    <!----//Contact-us-form---->
    <!-----Copy-right---------->
    <div class="footer_bottom">
        <div class="footer_bottom_left">
            <p><span style="color:silver"> <small class="copyright"> &copy; 2016 - <?php echo date('Y'); ?> <i
                                class="fa fa-heart"></i> <a
                                href="//cocis.news"> Cocis News</a> All rights reserved</small></p>
            </span>
        </div>
        <div class="footer_bottom_right">
            <ul>
                <li><a href="https://twitter.com/NewsCocis" target="_blank"><span class="fa fa-twitter"></span></a></li>
                <li><a href="https://fb.me/cocisnews" target="_blank"><span class="fa fa-facebook"></span></a></li>
                <li><a href="https://plus.google.com/u/1/communities/108139383578981516178" target="_blank"><span
                                class="fa fa-google-plus"></span></a></li>
                <li><a href="https://www.linkedin.com/" target="_blank"><span class="fa fa-linkedin"></span></a></li>
                <li><a href="https://www.pinterest.com/" target="_blank"><span class="fa fa-pinterest"></span></a></li>
            </ul>
        </div>
    </div>
    <!-----Copy-right---------->
</footer>
</div>
</div>
<!-----javascript---------->
<script src="<?php base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php base_url(); ?>assets/js/slick.min.js"></script>
<script src="<?php base_url(); ?>assets/js/jquery.li-scroller.1.0.js"></script>
<script src="<?php base_url(); ?>assets/js/custom.js"></script>
<script id="dsq-count-scr" src="//techninjaug.disqus.com/count.js" async></script>
</body>
</html>

<?php

if (isset($_POST['submit'])) {
    $user_title = $_POST['name'];
    $user_date = date('y-m-d');
    $user_email = $_POST['email'];
    $user_message = $_POST['message'];

    if ($user_title == '' or $user_email == '' or
        $user_message == '') {

        echo "<script>alert('please fill out all fields!!')</script>";

        exit();
    } else {

        $insert_query = "Insert into contact (user_title,user_date,user_email,user_message) 
		 values('$user_title','$user_date','$user_email','$user_message')";

        $result = mysqli_query($dbcon, $insert_query);

        if ($result) {

            echo "<script>alert('Your Message has been sent successfully...')</script>";
        }
    }

}

?>