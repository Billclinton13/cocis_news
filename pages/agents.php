<?php

include_once("includes/connect.php");

function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //Deteksi Sistem Operasi
    if (preg_match('/linux|X11/i', $u_agent)) {
        $platform = 'Linux';
    } elseif (preg_match('/macintosh|mac os x|Mac_PowerPC/i', $u_agent)) {
        $platform = 'Mac OS';
    } elseif (preg_match('/Win16/i', $u_agent)) {
        $platform = 'Windows 3.11';
    } elseif (preg_match('/windows 95|Win95|Windows_95/i', $u_agent)) {
        $platform = 'Windows 95';
    } elseif (preg_match('/Windows 98|Win98/i', $u_agent)) {
        $platform = 'Windows 98';
    } elseif (preg_match('/Windows NT 5.0|Windows 2000/i', $u_agent)) {
        $platform = 'Windows 2000';
    } elseif (preg_match('/Windows NT 5.1|Windows XP/i', $u_agent)) {
        $platform = 'Windows XP';
    } elseif (preg_match('/Windows NT 5.2/i', $u_agent)) {
        $platform = 'Windows Server 2003';
    } elseif (preg_match('/Windows NT 6.0/i', $u_agent)) {
        $platform = 'Windows Vista';
    } elseif (preg_match('/Windows NT 6.1/i', $u_agent)) {
        $platform = 'Windows 7';
    } elseif (preg_match('/Windows NT 4.0|WinNT4.0|WinNT|Windows NT/i', $u_agent)) {
        $platform = 'Windows NT 4.0';
    } elseif (preg_match('/Windows ME/i', $u_agent)) {
        $platform = 'Windows ME';
    } elseif (preg_match('/OpenBSD/i', $u_agent)) {
        $platform = 'Open BSD';
    } elseif (preg_match('/SunOS/i', $u_agent)) {
        $platform = 'Sun OS';
    } elseif (preg_match('/QNX/i', $u_agent)) {
        $platform = 'QNX';
    } elseif (preg_match('/BeOS/i', $u_agent)) {
        $platform = 'BeOS';
    } elseif (preg_match('/OS\/2/i', $u_agent)) {
        $platform = 'OS/2';
    } elseif (preg_match('/Googlebot/i', $u_agent)) {
        $platform = 'Google Bot';
    } elseif (preg_match('/MSNBot/i', $u_agent)) {
        $platform = 'MSN Bot';
    } elseif (preg_match('/Yammybot/i', $u_agent)) {
        $platform = 'Yahoo Bot';
    } elseif (preg_match('/nuhk|Openbot|Slurp|Ask Jeeves\/Teoma|ia_archiver/i', $u_agent)) {
        $platform = 'Search Bot';
    }
    $ub = "";
    //Deteksi Browser
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // Deteksi versi browser
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // jika tidak ada yang cocok, lanjutkan
    }

    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern

    );
}

$ip = $_SERVER['REMOTE_ADDR'];
$ip_num = sprintf("%u", ip2long($ip));
$_q = "SELECT code, country_cod, country_name FROM countries WHERE " . $ip_num . " BETWEEN ip_from AND ip_to";
$_qry = mysqli_query($dbcon, $_q);
$qry = mysqli_fetch_array($_qry);
$flg = strtolower($qry['code']);
$country = $qry['country_cod'];
$nctr = $qry['country_name'];
?>